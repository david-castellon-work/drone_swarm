#!/bin/bash

SERIAL_PORT_PREFIX="/dev/serial/by-id/"
SERIAL_PORT_NAME=$(ls $SERIAL_PORT_PREFIX)

echo "$SERIAL_PORT_PREFIX$SERIAL_PORT_NAME" > serial_port.txt
