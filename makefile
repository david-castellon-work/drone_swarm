CC             := g++

# Source Directory
S_DIR        := src
# Dependency Directory
D_DIR        := dep
# Internal Dependency Folder Name
ID_NAME      := int
# External Dependency Folder Name
EXT_D_NAME   := ext
# Internal Dependency Directories
ID_DIR       := $(D_DIR)/$(ID_NAME)

# Unit test
U_DIR        := unit_tests

# Build Directory
B_DIR        := build
# Target Directory
T_DIR        := bin

# Extensions
SRCEXT       := cpp
OBJEXT       := o

# Compiler Flags and Dependencies -DNDEBUG
CFLAGS       := -g -Wall -Werror -Wfatal-errors -Wno-address-of-packed-member -Wno-unused-variable -Wno-unused-but-set-variable -DNDEBUG
LIB          := -lm -lpthread -I$(D_DIR)
INC          := -I$(D_DIR)
INCDEP       := -I$(D_DIR)

# Target Names
T1           := drone_swarm
T2           := mavlink_serial_printer
T3           := test_flight

# Drone Swarm Source Directory
SRC1_DIR     := $(S_DIR)/$(T1)
# Mavlink Serial Print Target Directory
SRC2_DIR     := $(S_DIR)/$(T2)

# Internal Dependency Sources and Objects List
ID_SRC       := $(shell find $(ID_DIR) -type f -name *.$(SRCEXT))
ID_OBJ       := $(patsubst $(ID_DIR)/%,$(B_DIR)/$(ID_DIR)/%,$(ID_SRC:.$(SRCEXT)=.$(OBJEXT)))

# Target 1 Sources and Objects List
SRC1         := $(shell find $(SRC1_DIR) -type f -name *.$(SRCEXT))
OBJ1         := $(patsubst $(SRC1_DIR)/%,$(B_DIR)/$(SRC1_DIR)/%,$(SRC1:.$(SRCEXT)=.$(OBJEXT)))

# Target 2 Sources and Objects List
SRC2         := $(shell find $(SRC2_DIR) -type f -name *.$(SRCEXT))
OBJ2         := $(patsubst $(SRC2_DIR)/%,$(B_DIR)/$(SRC2_DIR)/%,$(SRC2:.$(SRCEXT)=.$(OBJEXT)))


all: directories git_submodule $(T1) $(T2)
# drone_swarm: $(T1)
# mavlink_serial_printer: $(T2)


directories:
	@echo "Creating Directories!"
	@echo "--------------------------------------------------"
	@echo "SRC1"
	@echo $(SRC1)
	@echo "SRC2"
	@echo $(SRC2)
	@echo "OBJ1"
	@echo $(OBJ1)
	@echo "OBJ2"
	@echo $(OBJ2)
	@echo "--------------------------------------------------"
	mkdir -p $(T_DIR)
	mkdir -p $(B_DIR)
	@echo "--------------------------------------------------"

git_submodule:
	git submodule update --init --recursive

$(B_DIR)/$(ID_DIR)/%.$(OBJEXT): $(ID_DIR)/%.$(SRCEXT)
	@echo "Creating Objects for Internal Dependencies!"
	@echo "--------------------------------------------------"
	@echo "$@"
	@echo $@
	@echo "--------------------------------------------------"
	@echo "$<"
	@echo $<
	@echo "--------------------------------------------------"
	@echo "Creating Subdirectories for Objects!"
	mkdir -p $(dir $@)
	@echo "--------------------------------------------------"
	@echo "Compiling!"
	$(CC) $(CFLAGS) $(INC) -c -o $@ $<
	@echo "finished creating object" $@
	@echo "--------------------------------------------------"

# Compiles target 1's source files.
$(B_DIR)/$(SRC1_DIR)/%.$(OBJEXT): $(SRC1_DIR)/%.$(SRCEXT)
	@echo "--------------------------------------------------"
	@echo "Attempting To Create Object!"
	mkdir -p $(dir $@)
	@echo $@
	@echo $<
	@echo "--------------------------------------------------"
	$(CC) $(CFLAGS) $(INC) -c -o $@ $<
	@echo "finished creating object" $@
	@echo "--------------------------------------------------"

# Compiles target 1's source files.
$(B_DIR)/$(SRC2_DIR)/%.$(OBJEXT): $(SRC2_DIR)/%.$(SRCEXT)
	@echo "--------------------------------------------------"
	@echo "Attempting To Create Object!"
	mkdir -p $(dir $@)
	@echo $@
	@echo $<
	@echo "--------------------------------------------------"
	$(CC) $(CFLAGS) $(INC) -c -o $@ $<
	@echo "finished creating object" $@
	@echo "--------------------------------------------------"

# Performs linking
$(T1): $(OBJ1) $(ID_OBJ)
	@echo "Attempting To Link!"
	@echo $(SRC1)
	@echo $(SRC2)
	@echo $(OBJ1)
	@echo $(OBJ2)
	@echo "--------------------------------------------------"
	$(CC) -o $(T_DIR)/drone_swarm $^ $(LIB)
	@echo "--------------------------------------------------"

# Performs linking
$(T2): $(OBJ2) $(ID_OBJ)
	@echo "Attempting To Link!"
	@echo $(SRC1)
	@echo $(SRC2)
	@echo $(OBJ1)
	@echo $(OBJ2)
	@echo "--------------------------------------------------"
	$(CC) -o $(T_DIR)/mavlink_serial_printer $^ $(LIB)
	@echo "--------------------------------------------------"


clean:
	@echo "Cleaning!"
	rm -rf build
	rm -rf bin
