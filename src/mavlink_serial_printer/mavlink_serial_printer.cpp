#include "mavlink_serial_printer.h"

// char uart_name[] = "/dev/serial/by-id//usb-Prolific_Technology_Inc._USB-Serial_Controller-if00-port0";
// char uart_name[] = "/dev/serial/by-id/usb-ArduPilot_fmuv2_2C0054000D51353136343335-if00";
char uart_name[] = "/dev/serial/by-id/usb-Silicon_Labs_CP2102_USB_to_UART_Bridge_Controller_0001-if00-port0";
int baudrate = 57600;

void parse_commandline(
	int     argc        ,
	char ** argv        ,
	char *  uart_name   ,
	int  &  baudrate    ,
	bool &  autotakeoff
)
{

	// string for command line usage
	const char *COMMANDLINE_USAGE = "usage: mavlink_control [-d <devicename> -b <baudrate>] [-a ]";

	// Read input arguments
	for (int i = 1; i < argc; i++) { // argv[0] is "mavlink"

		// Help
		if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0) {
			printf("%s\n",COMMANDLINE_USAGE);
			throw EXIT_FAILURE;
		}

		// UART device ID
		if (strcmp(argv[i], "-d") == 0 || strcmp(argv[i], "--device") == 0) {
			if (argc > i + 1) {
				i++;
				// switches the uart
				uart_name = argv[i];
			} else {
				printf("%s\n",COMMANDLINE_USAGE);
				throw EXIT_FAILURE;
			}
		}

		// Baud rate
		if (strcmp(argv[i], "-b") == 0 || strcmp(argv[i], "--baud") == 0) {
			if (argc > i + 1) {
				i++;
				baudrate = atoi(argv[i]);
			} else {
				printf("%s\n",COMMANDLINE_USAGE);
				throw EXIT_FAILURE;
			}
		}

		// Autotakeoff
		if (strcmp(argv[i], "-a") == 0 || strcmp(argv[i], "--autotakeoff") == 0) {
			autotakeoff = true;
		}

	}
	return;
}



int main(int argc, char **argv)
{
	try {
		bool autotakeoff = false;

		// do the parse, will throw an int if it fails
		parse_commandline(argc, argv, uart_name, baudrate, autotakeoff);

		// Responsible for reading/sending mavlink from/to px4.
		// Guarded with pthread mutex lock.
		Generic_Port *port;
		port = new Serial_Port(uart_name, baudrate);
		port->start();

		Reader *reader = new Reader(port);
		reader->print_messages();
	} catch ( int error ) {
		fprintf(stderr, "Mavlink Printer Error: %d", error);
		return error;
	}
}
