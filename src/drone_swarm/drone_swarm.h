#ifndef _DRONE_SWARM_H_
#define _DRONE_SWARM_H_

#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <unistd.h>
#include <cmath>
#include <string.h>
#include <inttypes.h>
#include <fstream>
#include <signal.h>
#include <time.h>
#include <sys/time.h>

// #include <ext/mavlink/common/mavlink.h>

#include <int/autopilot/autopilot_interface.h>
#include <int/mavlink/port/serial_port.h>
#include <int/mavlink/port/udp_port.h>
#include <int/debug.h>

using std::string;
using namespace std;

// ------------------------------------------------------------------------------
//   Prototypes
// ------------------------------------------------------------------------------

int main(int argc, char **argv);
int top(int argc, char **argv);

void commands(Autopilot_Interface &autopilot_interface, bool autotakeoff);
void parse_commandline(
	int argc,
	char **argv,
	char *uart_name,
	int &baudrate,
	bool &autotakeoff
);

// quit handler
Autopilot_Interface *autopilot_interface_quit;
Generic_Port *port_quit;
void quit_handler( int sig );

#endif
