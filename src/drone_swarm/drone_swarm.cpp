#include "drone_swarm.h"

// char uart_name[] = "/dev/serial/by-id/usb-ArduPilot_fmuv2_2C0054000D51353136343335-if00";
char uart_name[] = "/dev/serial/by-id/usb-Silicon_Labs_CP2102_USB_to_UART_Bridge_Controller_0001-if00-port0";
int baudrate     = 57600;

int top (int argc, char **argv)
{	bool autotakeoff = false;

	// do the parse, will throw an int if it fails
	parse_commandline(argc, argv, uart_name, baudrate, autotakeoff);

	// Responsible for reading/sending mavlink from/to px4.
	// Guarded with pthread mutex lock.
	Generic_Port *port;
	port = new Serial_Port(uart_name, baudrate);

	Autopilot_Interface autopilot_interface(port);

	port_quit = port;
	autopilot_interface_quit = &autopilot_interface;
	signal(SIGINT,quit_handler);

	port->start();
	printf("Before Autopilot Start!\n");
	autopilot_interface.start();


	printf("Before Commands!\n");
	commands(autopilot_interface, autotakeoff);

	autopilot_interface.stop();
	port->stop();

	delete port;
	return 0;

}


void commands(Autopilot_Interface &api, bool autotakeoff)
{

	// --------------------------------------------------------------------------
	//   START OFFBOARD MODE
	// --------------------------------------------------------------------------

	api.enable_offboard_control();
	usleep(100); // give some time to let it sink in

	// now the autopilot is accepting setpoint commands

	if(autotakeoff)
	{
		// arm autopilot
		api.arm_disarm(true);
		usleep(100); // give some time to let it sink in
	}

	// --------------------------------------------------------------------------
	//   SEND OFFBOARD COMMANDS
	// --------------------------------------------------------------------------
	printf("SEND OFFBOARD COMMANDS\n");

	// initialize command data strtuctures
	mavlink_set_position_target_local_ned_t sp;
	mavlink_set_position_target_local_ned_t ip = api.initial_position;

	// autopilot_interface.h provides some helper functions to build the command

	printf("Setting Drone Position!\n");
	// Example 1 - Fly up by to 2m
	set_position(
		ip.x       , // [m]
		ip.y       , // [m]
		ip.z - 2.0 , // [m]
		sp
	);

	if(autotakeoff)
	{
		sp.type_mask |= MAVLINK_MSG_SET_POSITION_TARGET_LOCAL_NED_TAKEOFF;
	}

	// SEND THE COMMAND
	api.update_setpoint(sp);
	// NOW pixhawk will try to move

	// Wait for 8 seconds, check position
	for (int i=0; i < 8; i++) {
		mavlink_local_position_ned_t pos = api.current_messages.local_position_ned;
		printf("Waited For %ds!\n", i);
		printf("%i CURRENT POSITION XYZ = [ % .4f , % .4f , % .4f ] \n", i, pos.x, pos.y, pos.z);
		sleep(1);
	}

	printf("--------------------------------------------------\n");

	printf("Updating Velocity and Yaw!\n");

	// Example 2 - Set Velocity
	set_velocity(
		-1.0 , // [m/s]
		-1.0 , // [m/s]
		 0.0 , // [m/s]
		  sp
	);

	// Example 2.1 - Append Yaw Command
	set_yaw(
		ip.yaw + 90.0/180.0*M_PI, // [rad]
		sp
	);

	// SEND THE COMMAND
	api.update_setpoint(sp);
	// NOW pixhawk will try to move

	// Wait for 4 seconds, check position
	for (int i=0; i < 4; i++)
	{
		mavlink_local_position_ned_t pos = api.current_messages.local_position_ned;
		printf("%ds\n", i);
		printf("%i CURRENT POSITION XYZ = [ % .4f , % .4f , % .4f ] \n", i, pos.x, pos.y, pos.z);
		sleep(1);
	}

	if(autotakeoff)
	{
		printf("Attempting to Land!\n");
		// Example 3 - Land using fixed velocity
		set_velocity(
			0.0       , // [m/s]
			0.0       , // [m/s]
			1.0       , // [m/s]
			sp
		);

		sp.type_mask |= MAVLINK_MSG_SET_POSITION_TARGET_LOCAL_NED_LAND;

		// SEND THE COMMAND
		api.update_setpoint(sp);
		// NOW pixhawk will try to move

		// Wait for 8 seconds, check position
		for (int i=0; i < 8; i++)
		{
			mavlink_local_position_ned_t pos = api.current_messages.local_position_ned;
			printf("%i CURRENT POSITION XYZ = [ % .4f , % .4f , % .4f ] \n", i, pos.x, pos.y, pos.z);
			sleep(1);
		}

		debug("\n");

		// disarm autopilot
		api.arm_disarm(false);
		usleep(100); // give some time to let it sink in
	}

	api.disable_offboard_control();

	// now pixhawk isn't listening to setpoint commands

	printf("READ SOME MESSAGES \n");

	// copy current messages
	Mavlink_Messages messages = api.current_messages;

	// local position in ned frame
	mavlink_local_position_ned_t pos = messages.local_position_ned;
	printf("Got message LOCAL_POSITION_NED (spec: https://mavlink.io/en/messages/common.html#LOCAL_POSITION_NED)\n");
	printf("    pos  (NED):  %f %f %f (m)\n", pos.x, pos.y, pos.z );

	// hires imu
	mavlink_highres_imu_t imu = messages.highres_imu;
	printf("Got message HIGHRES_IMU (spec: https://mavlink.io/en/messages/common.html#HIGHRES_IMU)\n");
	printf("    ap time:     %lu \n", imu.time_usec);
	printf("    acc  (NED):  % f % f % f (m/s^2)\n", imu.xacc , imu.yacc , imu.zacc );
	printf("    gyro (NED):  % f % f % f (rad/s)\n", imu.xgyro, imu.ygyro, imu.zgyro);
	printf("    mag  (NED):  % f % f % f (Ga)\n"   , imu.xmag , imu.ymag , imu.zmag );
	printf("    baro:        %f (mBar) \n"  , imu.abs_pressure);
	printf("    altitude:    %f (m) \n"     , imu.pressure_alt);
	printf("    temperature: %f C \n"       , imu.temperature );

	printf("\n");


	return;

}


void parse_commandline(
	int     argc        ,
	char ** argv        ,
	char *  uart_name   ,
	int  &  baudrate    ,
	bool &  autotakeoff
)
{

	// string for command line usage
	const char *COMMANDLINE_USAGE = "usage: mavlink_control [-d <devicename> -b <baudrate>] [-a ]";

	// Read input arguments
	for (int i = 1; i < argc; i++) { // argv[0] is "mavlink"

		// Help
		if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0) {
			debug("%s\n",COMMANDLINE_USAGE);
			throw EXIT_FAILURE;
		}

		// UART device ID
		if (strcmp(argv[i], "-d") == 0 || strcmp(argv[i], "--device") == 0) {
			if (argc > i + 1) {
				i++;
				// switches the uart
				uart_name = argv[i];
			} else {
				debug("%s\n",COMMANDLINE_USAGE);
				throw EXIT_FAILURE;
			}
		}

		// Baud rate
		if (strcmp(argv[i], "-b") == 0 || strcmp(argv[i], "--baud") == 0) {
			if (argc > i + 1) {
				i++;
				baudrate = atoi(argv[i]);
			} else {
				debug("%s\n",COMMANDLINE_USAGE);
				throw EXIT_FAILURE;
			}
		}

		// Autotakeoff
		if (strcmp(argv[i], "-a") == 0 || strcmp(argv[i], "--autotakeoff") == 0) {
			autotakeoff = true;
		}

	}
	return;
}


// Called when you press Ctrl-C
void quit_handler( int sig )
{
	debug("\n");
	debug("TERMINATING AT USER REQUEST\n");
	debug("\n");

	// autopilot interface
	try {
		autopilot_interface_quit->handle_quit(sig);
	}
	catch (int error){}

	// port
	try {
		port_quit->stop();
	}
	catch (int error){}

	// end program here
	exit(0);
}


int main(int argc, char **argv)
{
	bool autotakeoff = false;

	// This program uses throw, wrap one big try/catch here
	try
	{
		int result = top(argc,argv);
		return result;
	}

	catch ( int error )
	{
		fprintf(stderr,"Drone Swarm Error: %i\n" , error);
		return error;
	}

}
