#ifndef MAVLINK_SERIAL_PRINTER_H_
#define MAVLINK_SERIAL_PRINTER_H_

#include <signal.h>
#include <stdio.h>

#include <int/mavlink/port/generic_port.h>
#include <int/mavlink/helpers/helpers.h>

class Reader
{
public:
	Reader();
	Reader(Generic_Port *port_);

	// Retrieves System Id
	// Retrieves Autopilot Id

	// Retrieves position
	// int get_position();

	void print_message();
	void print_messages();
	// void handle_quit( int sig );

private:

	Generic_Port *port;
	Mavlink_Messages current_messages;
	bool time_to_exit;
};

#endif
