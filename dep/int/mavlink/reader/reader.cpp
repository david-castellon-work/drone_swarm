#include "reader.h"


Reader::Reader(Generic_Port *port_)
{
	time_to_exit = false;

	port = port_;
}

void Reader::print_message()
{
	bool success;               // receive success flag
	bool received_all = false;  // receive only one message
	Time_Stamps this_timestamps;

	if ( !port->is_running() )
	{
		fprintf(stderr,"Reader::print_message()\n");
		fprintf(stderr,"Serial Port Not Running!\n");
		throw 1;
	}

	printf("Start Reading!\n");

	// Blocking wait for new data
	while ( !received_all and !time_to_exit )
	{
		mavlink_message_t message;
		success = port->read_message(message);

		if( success )
		{

			// Store message sysid and compid.
			// Note this doesn't handle multiple message sources.
			current_messages.sysid  = message.sysid;
			current_messages.compid = message.compid;
			uint64_t current_time = get_time_usec();

			printf("Mavlink Message ID: %i\n",message.msgid);
			// printf("Time After Start in Microseconds: %i\n",current_time); 
			print_time(current_time);
			printf("\n");

			// Handle Message ID
			switch (message.msgid)
			{
				case MAVLINK_MSG_ID_HEARTBEAT:
				{
					printf("MAVLINK_MSG_ID_HEARTBEAT Received at ");

					mavlink_msg_heartbeat_decode(&message, &(current_messages.heartbeat));
					current_messages.time_stamps.heartbeat = current_time;
					this_timestamps.heartbeat = current_messages.time_stamps.heartbeat;

					printf("custom_mode = %d\n"     , current_messages.heartbeat.custom_mode);
					printf("type = %d\n"            , current_messages.heartbeat.type);
					printf("autopilot = %d\n"       , current_messages.heartbeat.autopilot);
					printf("base_mode = %d\n"       , current_messages.heartbeat.base_mode);
					printf("system_status = %d\n"   , current_messages.heartbeat.system_status);
					printf("mavlink_version = %d\n" , current_messages.heartbeat.mavlink_version);

					break;
				}

				case MAVLINK_MSG_ID_SYS_STATUS:
				{
					printf("MAVLINK_MSG_ID_SYS_STATUS\n");

					mavlink_msg_sys_status_decode(&message, &(current_messages.sys_status));
					current_messages.time_stamps.sys_status = current_time;
					this_timestamps.sys_status = current_messages.time_stamps.sys_status;

					printf("onboard_control_sensors_present = %d\n"     , current_messages.sys_status.onboard_control_sensors_present);
					printf("onboard_control_sensors_enabled = %d\n"            , current_messages.sys_status.onboard_control_sensors_enabled);
					printf("onboard_control_sensors_health = %d\n"       , current_messages.sys_status.onboard_control_sensors_health);
					printf("load = %d\n"       , current_messages.sys_status.load);
					printf("voltage_battery = %d\n"   , current_messages.sys_status.voltage_battery);
					printf("current_battery = %d\n" , current_messages.sys_status.current_battery);
					printf("drop_rate_comm = %d\n"     , current_messages.sys_status.drop_rate_comm);
					printf("errors_comm = %d\n"            , current_messages.sys_status.errors_comm);
					printf("errors_count1 = %d\n"       , current_messages.sys_status.errors_count1);
					printf("errors_count2 = %d\n"       , current_messages.sys_status.errors_count2);
					printf("errors_count3 = %d\n"   , current_messages.sys_status.errors_count3);
					printf("errors_count4 = %d\n" , current_messages.sys_status.errors_count4);
					printf("battery_remaining = %d\n" , current_messages.sys_status.battery_remaining);

					break;
				}

				case MAVLINK_MSG_ID_BATTERY_STATUS:
				{
					printf("MAVLINK_MSG_ID_BATTERY_STATUS\n");

					mavlink_msg_battery_status_decode(&message, &(current_messages.battery_status));
					current_messages.time_stamps.battery_status = current_time;
					this_timestamps.battery_status = current_messages.time_stamps.battery_status;

					printf("current_consumed = %d\n"     , current_messages.battery_status.current_consumed);
					printf("energy_consumed = %d\n"            , current_messages.battery_status.energy_consumed);
					printf("temperature = %d\n"       , current_messages.battery_status.temperature);
					printf("voltages = %d\n"       , current_messages.battery_status.voltages[1]);
					printf("current_battery = %d\n"   , current_messages.battery_status.current_battery);
					printf("id = %d\n" , current_messages.battery_status.id);
					printf("battery_function = %d\n"     , current_messages.battery_status.battery_function);
					printf("type = %d\n"            , current_messages.battery_status.type);
					printf("battery_remaining = %d\n"       , current_messages.battery_status.battery_remaining);
					printf("time_remaining = %d\n"       , current_messages.battery_status.time_remaining);
					printf("charge_state = %d\n"   , current_messages.battery_status.charge_state);
					printf("voltages_ext = %d\n" , current_messages.battery_status.voltages_ext[1]);
					printf("mode = %d\n" , current_messages.battery_status.mode);
					printf("fault_bitmask = %d\n" , current_messages.battery_status.fault_bitmask);

					break;
				}

				case MAVLINK_MSG_ID_RADIO_STATUS:
				{
					printf("MAVLINK_MSG_ID_RADIO_STATUS\n");

					mavlink_msg_radio_status_decode(&message, &(current_messages.radio_status));
					current_messages.time_stamps.radio_status = current_time;
					this_timestamps.radio_status = current_messages.time_stamps.radio_status;

					printf("rxerrors = %d\n"     , current_messages.radio_status.rxerrors);
					printf("fixed = %d\n"            , current_messages.radio_status.fixed);
					printf("rssi = %d\n"       , current_messages.radio_status.rssi);
					printf("remrssi = %d\n"       , current_messages.radio_status.remrssi);
					printf("txbuf = %d\n"   , current_messages.radio_status.txbuf);
					printf("noise = %d\n" , current_messages.radio_status.noise);
					printf("remnoise = %d\n"     , current_messages.radio_status.remnoise);
					
					break;
				}

				case MAVLINK_MSG_ID_LOCAL_POSITION_NED:
				{
					printf("MAVLINK_MSG_ID_LOCAL_POSITION_NED\n");

					mavlink_msg_local_position_ned_decode(&message, &(current_messages.local_position_ned));
					current_messages.time_stamps.local_position_ned = current_time;
					this_timestamps.local_position_ned = current_messages.time_stamps.local_position_ned;

					printf("time_boot_ms = %" PRIu32 "\n" , current_messages.local_position_ned.time_boot_ms);
					printf("x = %f\n", current_messages.local_position_ned.x);
					printf("y = %f\n", current_messages.local_position_ned.y);
					printf("z = %f\n", current_messages.local_position_ned.z);
					printf("vx = %f\n", current_messages.local_position_ned.vx);
					printf("vy = %f\n", current_messages.local_position_ned.vy);
					printf("vz = %f\n", current_messages.local_position_ned.vz);

					break;
				}

				case MAVLINK_MSG_ID_GLOBAL_POSITION_INT:
				{
					printf("MAVLINK_MSG_ID_GLOBAL_POSITION_INT\n");

					mavlink_msg_global_position_int_decode(&message, &(current_messages.global_position_int));
					current_messages.time_stamps.global_position_int = current_time;
					this_timestamps.global_position_int = current_messages.time_stamps.global_position_int;

					printf("time_boot_ms = %d\n"     , current_messages.global_position_int.time_boot_ms);
					printf("lat = %d\n"            , current_messages.global_position_int.lat);
					printf("lon = %d\n"       , current_messages.global_position_int.lon);
					printf("alt = %d\n"       , current_messages.global_position_int.alt);
					printf("relative_alt = %d\n"   , current_messages.global_position_int.relative_alt);
					printf("vx = %d\n"   , current_messages.global_position_int.vx);
					printf("vy = %d\n" , current_messages.global_position_int.vy);
					printf("vz = %d\n"     , current_messages.global_position_int.vz);
					printf("hdg = %d\n" , current_messages.global_position_int.hdg);

					break;
				}

				case MAVLINK_MSG_ID_POSITION_TARGET_LOCAL_NED:
				{
					printf("MAVLINK_MSG_ID_POSITION_TARGET_LOCAL_NED\n");

					mavlink_msg_position_target_local_ned_decode(&message, &(current_messages.position_target_local_ned));
					current_messages.time_stamps.position_target_local_ned = current_time;
					this_timestamps.position_target_local_ned = current_messages.time_stamps.position_target_local_ned;

					printf("time_boot_ms = %" PRIu32     , current_messages.position_target_local_ned.time_boot_ms);
					printf("x = %f\n"            , current_messages.position_target_local_ned.x);
					printf("y = %f\n"       , current_messages.position_target_local_ned.y);
					printf("z = %f\n"       , current_messages.position_target_local_ned.z);
					printf("vx = %f\n"   , current_messages.position_target_local_ned.vx);
					printf("vy = %f\n" , current_messages.position_target_local_ned.vy);
					printf("vz = %f\n"     , current_messages.position_target_local_ned.vz);
					printf("afx = %f\n"     , current_messages.position_target_local_ned.afx);
					printf("afy = %f\n"            , current_messages.position_target_local_ned.afy);
					printf("afz = %f\n"       , current_messages.position_target_local_ned.afz);
					printf("yaw = %f\n"       , current_messages.position_target_local_ned.yaw);
					printf("yaw_rate = %f\n"   , current_messages.position_target_local_ned.yaw_rate);
					printf("type_mask = %" PRIu16 "\n" , current_messages.position_target_local_ned.type_mask);
					printf("coordinate_frame = %" PRIu8 "\n" , current_messages.position_target_local_ned.coordinate_frame);

					break;
				}

				case MAVLINK_MSG_ID_POSITION_TARGET_GLOBAL_INT:
				{
					printf("MAVLINK_MSG_ID_POSITION_TARGET_GLOBAL_INT\n");

					mavlink_msg_position_target_global_int_decode(&message, &(current_messages.position_target_global_int));
					current_messages.time_stamps.position_target_global_int = current_time;
					this_timestamps.position_target_global_int = current_messages.time_stamps.position_target_global_int;

					printf("time_boot_ms = %" PRIu32 "\n"  , current_messages.position_target_global_int.time_boot_ms);
					printf("lat_int = %" PRId32 "\n"           , current_messages.position_target_global_int.lat_int);
					printf("lon_int = %" PRId32 "\n"      , current_messages.position_target_global_int.lon_int);
					printf("alt = %f\n"       , current_messages.position_target_global_int.alt);
					printf("vx = %f\n"   , current_messages.position_target_global_int.vx);
					printf("vy = %f\n" , current_messages.position_target_global_int.vy);
					printf("vz = %f\n"     , current_messages.position_target_global_int.vz);
					printf("afx = %f\n"     , current_messages.position_target_global_int.afx);
					printf("afy = %f\n"            , current_messages.position_target_global_int.afy);
					printf("afz = %f\n"       , current_messages.position_target_global_int.afz);
					printf("yaw = %f\n"       , current_messages.position_target_global_int.yaw);
					printf("yaw_rate = %f\n"   , current_messages.position_target_global_int.yaw_rate);
					printf("type_mask = %" PRIu16 "\n", current_messages.position_target_global_int.type_mask);
					printf("coordinate_frame = %" PRIu8 "\n" , current_messages.position_target_global_int.coordinate_frame);

					break;
				}

//				case MAVLINK_MSG_ID_HIGHRES_IMU2:
//				{
//					printf("MAVLINK_MSG_ID_HIGHRES_IMU\n");
//
//					mavlink_msg_highres_imu_decode(&message, &(current_messages.highres_imu));
//					current_messages.time_stamps.highres_imu = current_time;
//					this_timestamps.highres_imu = current_messages.time_stamps.highres_imu;
//
//					printf("time_usec = %" PRIu64    , current_messages.highres_imu.time_usec);
//					printf("xacc = %f\n"            , current_messages.highres_imu.xacc);
//					printf("yacc = %f\n"       , current_messages.highres_imu.yacc);
//					printf("zacc = %f\n"       , current_messages.highres_imu.zacc);
//					printf("xgyro = %f\n"   , current_messages.highres_imu.xgyro);
//					printf("ygyro = %f\n" , current_messages.highres_imu.ygyro);
//					printf("zgyro = %f\n"     , current_messages.highres_imu.zgyro);
//					printf("xmag = %f\n"     , current_messages.highres_imu.xmag);
//					printf("ymag = %f\n"            , current_messages.highres_imu.ymag);
//					printf("zmag = %f\n"       , current_messages.highres_imu.zmag);
//					printf("abs_pressure = %f\n"       , current_messages.highres_imu.abs_pressure);
//					printf("diff_pressure = %f\n"   , current_messages.highres_imu.diff_pressure);
//					printf("pressure_alt = %f\n" , current_messages.highres_imu.pressure_alt);
//					printf("temperature = %f\n"     , current_messages.highres_imu.temperature);
//					printf("fields_updated = %" PRIu16 "\n"   , current_messages.highres_imu.fields_updated);
//					printf("id = %" PRIu8 "\n"    , current_messages.highres_imu.id);
//
//					break;
//				}

				case MAVLINK_MSG_ID_ATTITUDE:
				{
					printf("MAVLINK_MSG_ID_ATTITUDE\n");

					mavlink_msg_attitude_decode(&message, &(current_messages.attitude));
					current_messages.time_stamps.attitude = current_time;
					this_timestamps.attitude = current_messages.time_stamps.attitude;

					printf("time_boot_ms = %" PRIu32 "\n"    , current_messages.attitude.time_boot_ms);
					printf("roll = %f\n"            , current_messages.attitude.roll);
					printf("pitch = %f\n"       , current_messages.attitude.pitch);
					printf("yaw = %f\n"       , current_messages.attitude.yaw);
					printf("rollspeed = %f\n"   , current_messages.attitude.rollspeed);
					printf("pitchspeed = %f\n"   , current_messages.attitude.pitchspeed);
					printf("yawspeed = %f\n" , current_messages.attitude.yawspeed);

					break;
				}

				default:
				{
					// printf("Warning, did not handle message id %i\n",message.msgid);
					break;
				}

			} // end: switch msgid
			printf("--------------------------------------------------\n");

		} // end: if read message

		// Check for receipt of all items
		received_all =
				this_timestamps.heartbeat                  &&
				this_timestamps.battery_status             &&
				this_timestamps.radio_status               &&
				this_timestamps.local_position_ned         &&
				this_timestamps.global_position_int        &&
				this_timestamps.position_target_local_ned  &&
				this_timestamps.position_target_global_int &&
				this_timestamps.highres_imu                &&
				this_timestamps.attitude                   &&
				this_timestamps.sys_status
				;


	} // end: while not received all

	return;
}
// void Reader::handle_quit(int sig)
// {
// }
void Reader::print_messages()
{
	while ( 1 ) print_message();
}

// void Reader::print_messages_thread()
// {
// }
// void Reader::get_system_id()
// {
// 	printf("LOOKING FOR SYSTEM ID!\n");
// 	while ( not current_messages.sysid )
// 	{
// 		if ( time_to_exit ) return;
// 		usleep(500000); // check at 2Hz
// 	}
// }
