#ifndef MAVLINK_WRITER_H_
#define MAVLINK_WRITER_H_

#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <pthread.h> // This uses POSIX Threads
#include <unistd.h>  // UNIX standard function definitions
#include <mutex>

#include <ext/mavlink/common/mavlink.h>

#include <int/debug.h>
#include <int/mavlink/port/generic_port.h>
#include <int/mavlink/helpers/helpers.h>

class Writer
{

	public:
		uint64_t write_count;

		Writer();
		Writer(Generic_Port *port_);
		~Writer();

		// Messages current_messages;

		mavlink_set_position_target_local_ned_t initial_position;
		void update_setpoint(mavlink_set_position_target_local_ned_t setpoint);
		int  write_message(mavlink_message_t message);

		int arm_disarm( bool flag );
		void enable_offboard_control();
		void disable_offboard_control();

		void start();
		void stop();

	private:

		Generic_Port *port;

		struct {
			std::mutex mutex;
			mavlink_set_position_target_local_ned_t data;
		} current_setpoint;

		int toggle_offboard_control(
			bool flag ,
			int system_id ,
			int autopilot_id,
			int companion_id
		);
		void write_setpoint(int system_id, int autopilot_id, int companion_id);
		bool time_to_exit;
};

#endif // MAVLINK_WRITER_H_
