// Error Handling Pattern
//
// 1. Call a function
// 2. Check if the return value is an error.
// 3. Clean up all resources
// 4. Print out a helpful error message.

#ifndef __dbg_h__
#define __dbg_h__

#include <stdio.h>
#include <errno.h>
#include <string.h> // strerror

// Debugging messages for developer.
#ifdef NDEBUG
	#define debug(M, ...)
#else
	#define debug(M, ...) \
		fprintf(stderr, \
			"DEBUG %s:%d: function: %s " M "\n", \
			__FILE__, \
			__LINE__, \
			__func__, \
		##__VA_ARGS__)
	// __FILE__ is the current file.
	// __LINE__ is the current line.
	// ##__VA_ARGS__ is the extra arguments in debug used for extra printf string placeholders.
#endif

// strerrror returns a string description of the error number.
#define clean_errno() (errno == 0 ? "None" : strerror(errno))

// Macros for logging messages meant for end user.
#define log_err(M, ...) \
	fprintf(stderr, \
		"[ERROR] (%s:%d: function: %s errno: %s) " M "\n", \
		__FILE__, \
		__LINE__, \
		__func__, \
		clean_errno(), \
	##__VA_ARGS__)

#define log_warn(M, ...) \
	fprintf(stderr, \
		"[ERROR] (%s:%d: function: %s: errno: %s) " M "\n", \
		__FILE__, \
		__LINE__, \
		__func__, \
		clean_errno(), \
	##__VA_ARGS__)


#define log_info(M, ...) \
	fprintf(stderr, \
		"[INFO] (%s:%d: function: %s) " M "\n", \
		__FILE__, \
		__LINE__, \
		__func__, \
	##__VA_ARGS__)

// If A is not true, log error and goto error cleanup code.
#define check(A, M, ...) \
	if(!(A)) { \
		log_err(M, ##__VA_ARGS__); \
		errno = 0; \
		goto error; \
	}

// Placed in code that shouldn't run.
// If it does, then goes to error.
#define sentinel(M, ...) { \
	log_err(M, ##__VA_ARGS__); \
	errno = 0; \
	goto error; \
}

// Checks to see if a pointer is valid. If it isn't prints message.
#define check_mem(A) check((A), "Out of memory.")

// In debug mode, prints and jumps to error handler
// In prod mode, does not print and jumps to error handler
#define check_debug(A, M, ...) \
	if(!(A)) { \
		debug(M, ##__VA_ARGS__); \
		errno = 0; \
		goto error; \
	}

#endif
