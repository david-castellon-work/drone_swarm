#include "autopilot_interface.h"

Autopilot_Interface::
Autopilot_Interface(Generic_Port *port_)
{
	// initialize attributes
	write_count = 0;

	reading_status = 0;      // whether the read thread is running
	writing_status = 0;      // whether the write thread is running
	control_status = 0;      // whether the autopilot is in offboard control mode
	time_to_exit   = false;  // flag to signal thread exit

	read_tid  = 0; // read thread id
	write_tid = 0; // write thread id

	system_id    = 0; // system id
	autopilot_id = 0; // autopilot component id
	companion_id = 0; // companion computer component id

	current_messages.sysid  = system_id;
	current_messages.compid = autopilot_id;

	port = port_; // port management object

}

Autopilot_Interface::
~Autopilot_Interface()
{}

void
Autopilot_Interface::
update_setpoint(mavlink_set_position_target_local_ned_t setpoint)
{
	std::lock_guard<std::mutex> lock(current_setpoint.mutex);
	current_setpoint.data = setpoint;
}

void
Autopilot_Interface::
read_messages()
{
	bool success;               // receive success flag
	bool received_all = false;  // receive only one message
	Time_Stamps this_timestamps;

	// Blocking wait for new data
	while ( !received_all and !time_to_exit )
	{
		mavlink_message_t message;
		success = port->read_message(message);

		// printf("UnSuccessful message!\n");
		if( success )
		{

			// Store message sysid and compid.
			// Note this doesn't handle multiple message sources.
			current_messages.sysid  = message.sysid;
			current_messages.compid = message.compid;
			// printf("Successful message!\n");

			// Handle Message ID
			switch (message.msgid)
			{

				case MAVLINK_MSG_ID_HEARTBEAT:
				{
					debug("MAVLINK_MSG_ID_HEARTBEAT\n");
					mavlink_msg_heartbeat_decode(&message, &(current_messages.heartbeat));
					current_messages.time_stamps.heartbeat = get_time_usec();
					this_timestamps.heartbeat = current_messages.time_stamps.heartbeat;
					printf("custom_mode = %u\n"     , current_messages.heartbeat.custom_mode);
					printf("type = %u\n"     , current_messages.heartbeat.type);
					printf("autopilot = %u\n"     , current_messages.heartbeat.autopilot);
					printf("base_mode = %u\n"     , current_messages.heartbeat.base_mode);
					printf("system_status = %u\n"     , current_messages.heartbeat.system_status);
					printf("mavlink_version = %u\n"     , current_messages.heartbeat.mavlink_version);
					break;
				}

				case MAVLINK_MSG_ID_SYS_STATUS:
				{
					debug("MAVLINK_MSG_ID_SYS_STATUS\n");
					mavlink_msg_sys_status_decode(&message, &(current_messages.sys_status));
					current_messages.time_stamps.sys_status = get_time_usec();
					this_timestamps.sys_status = current_messages.time_stamps.sys_status;
					printf("onboard_control_sensors_present = %u\n"     , current_messages.sys_status.onboard_control_sensors_present);
					printf("onboard_control_sensors_enabled = %u\n"     , current_messages.sys_status.onboard_control_sensors_enabled);
					printf("onboard_control_sensors_health = %u\n"     , current_messages.sys_status.onboard_control_sensors_health);
					printf("load = %u\n"     , current_messages.sys_status.load);
					printf("voltage_battery = %u\n"     , current_messages.sys_status.voltage_battery);
					printf("current_battery =  %" PRId16 "\n"     , current_messages.sys_status.current_battery);
					printf("drop_rate_comm = %u\n"     , current_messages.sys_status.drop_rate_comm);
					printf("errors_comm = %u\n"     , current_messages.sys_status.errors_comm);
					printf("errors_count1 = %u\n"     , current_messages.sys_status.errors_count1);
					printf("errors_count2 = %u\n"     , current_messages.sys_status.errors_count2);
					printf("errors_count3 = %u\n"     , current_messages.sys_status.errors_count3);
					printf("errors_count4 = %u\n"     , current_messages.sys_status.errors_count4);
					printf("battery_remaining =  %" PRId8 "\n"     , current_messages.sys_status.battery_remaining);
					break;
				}

				case MAVLINK_MSG_ID_BATTERY_STATUS:
				{
					debug("MAVLINK_MSG_ID_BATTERY_STATUS\n");
					mavlink_msg_battery_status_decode(&message, &(current_messages.battery_status));
					current_messages.time_stamps.battery_status = get_time_usec();
					this_timestamps.battery_status = current_messages.time_stamps.battery_status;
					printf("current_consumed = %" PRId32 "\n"     , current_messages.battery_status.current_consumed);
					printf("energy_consumed = %" PRId32 "\n"     , current_messages.battery_status.energy_consumed);
					printf("temperature = %" PRId16 "\n"     , current_messages.battery_status.temperature);
					printf("voltage = \n" );
					//print_uint16_t_array(current_messages.battery_status.voltages, sizeof(current_messages.battery_status.voltages) / sizeof(current_messages.battery_status.voltage[0]));
					printf("current_battery = %" PRId16 "\n"     , current_messages.battery_status.current_battery);
					printf("id = %u\n"     , current_messages.battery_status.id);
					printf("battery_function = %u\n"     , current_messages.battery_status.battery_function);
					printf("type = %u\n"     , current_messages.battery_status.type);
					printf("battery_remaining =  %" PRId8 "\n"     , current_messages.battery_status.battery_remaining);
					printf("time_remaining = %" PRId32 "\n"     , current_messages.battery_status.time_remaining);
					printf("charge_state = %u\n"     , current_messages.battery_status.charge_state);
					printf("voltages_ext = \n" );
					//print_uint16_t_array(current_messages.battery_status.voltages_ext, sizeof(current_messages.battery_status.voltages_ext) / sizeof(current_messages.battery_status.voltages_ext[0]));
					printf("mode = %u\n"     , current_messages.battery_status.mode);
					printf("fault_bitmask = %u\n"     , current_messages.battery_status.fault_bitmask);
					break;
				}

				case MAVLINK_MSG_ID_RADIO_STATUS:
				{
					debug("MAVLINK_MSG_ID_RADIO_STATUS\n");
					mavlink_msg_radio_status_decode(&message, &(current_messages.radio_status));
					current_messages.time_stamps.radio_status = get_time_usec();
					this_timestamps.radio_status = current_messages.time_stamps.radio_status;
					printf("rxerrors = %u\n"     , current_messages.radio_status.rxerrors);
					printf("fixed = %u\n"     , current_messages.radio_status.fixed);
					printf("rssi = %u\n"     , current_messages.radio_status.rssi);
					printf("remrssi = %u\n"     , current_messages.radio_status.remrssi);
					printf("txbuf = %u\n"     , current_messages.radio_status.txbuf);
					printf("noise = %u\n"     , current_messages.radio_status.noise);
					printf("remnoise = %u\n"     , current_messages.radio_status.remnoise);
					break;
				}

				case MAVLINK_MSG_ID_LOCAL_POSITION_NED:
				{
					debug("MAVLINK_MSG_ID_LOCAL_POSITION_NED\n");
					mavlink_msg_local_position_ned_decode(&message, &(current_messages.local_position_ned));
					current_messages.time_stamps.local_position_ned = get_time_usec();
					this_timestamps.local_position_ned = current_messages.time_stamps.local_position_ned;
					printf("time_boot_ms = %u\n"     , current_messages.local_position_ned.time_boot_ms);
					printf("x = %f\n"     , current_messages.local_position_ned.x);
					printf("y = %f\n"     , current_messages.local_position_ned.y);
					printf("z = %f\n"     , current_messages.local_position_ned.z);
					printf("vx = %f\n"     , current_messages.local_position_ned.vx);
					printf("vy = %f\n"     , current_messages.local_position_ned.vy);
					printf("vz = %f\n"     , current_messages.local_position_ned.vz);
					break;
				}

				case MAVLINK_MSG_ID_GLOBAL_POSITION_INT:
				{
					debug("MAVLINK_MSG_ID_GLOBAL_POSITION_INT\n");
					mavlink_msg_global_position_int_decode(&message, &(current_messages.global_position_int));
					current_messages.time_stamps.global_position_int = get_time_usec();
					this_timestamps.global_position_int = current_messages.time_stamps.global_position_int;
					printf("time_boot_ms = %u\n"     , current_messages.global_position_int.time_boot_ms);
					printf("lat = %" PRId32 "\n"     , current_messages.global_position_int.lat);
					printf("lon = %" PRId32 "\n"     , current_messages.global_position_int.lon);
					printf("alt = %" PRId32 "\n"     , current_messages.global_position_int.alt);
					printf("relative_alt = %" PRId32 "\n"     , current_messages.global_position_int.relative_alt);
					printf("vx = %" PRId16 "\n"     , current_messages.global_position_int.vx);
					printf("vy = %" PRId16 "\n"     , current_messages.global_position_int.vy);
					printf("vz = %" PRId16 "\n"     , current_messages.global_position_int.vz);
					printf("hdg = %u\n"     , current_messages.global_position_int.hdg);
					break;
				}

				case MAVLINK_MSG_ID_POSITION_TARGET_LOCAL_NED:
				{
					debug("MAVLINK_MSG_ID_POSITION_TARGET_LOCAL_NED\n");
					mavlink_msg_position_target_local_ned_decode(&message, &(current_messages.position_target_local_ned));
					current_messages.time_stamps.position_target_local_ned = get_time_usec();
					this_timestamps.position_target_local_ned = current_messages.time_stamps.position_target_local_ned;
					printf("time_boot_ms = %u\n"     , current_messages.position_target_local_ned.time_boot_ms);
					printf("x = %f\n"     , current_messages.position_target_local_ned.x);
					printf("y = %f\n"     , current_messages.position_target_local_ned.y);
					printf("z = %f\n"     , current_messages.position_target_local_ned.z);
					printf("vx = %f\n"     , current_messages.position_target_local_ned.vx);
					printf("vy = %f\n"     , current_messages.position_target_local_ned.vy);
					printf("vz = %f\n"     , current_messages.position_target_local_ned.vz);
					printf("afx = %f\n"     , current_messages.position_target_local_ned.afx);
					printf("afy = %f\n"     , current_messages.position_target_local_ned.afy);
					printf("afz = %f\n"     , current_messages.position_target_local_ned.afz);
					printf("yaw = %f\n"     , current_messages.position_target_local_ned.yaw);
					printf("yaw_rate = %f\n"     , current_messages.position_target_local_ned.yaw_rate);
					printf("type_mask = %u\n"     , current_messages.position_target_local_ned.type_mask);
					printf("coordinate_frame = %u\n"     , current_messages.position_target_local_ned.coordinate_frame);
					break;
				}

				case MAVLINK_MSG_ID_POSITION_TARGET_GLOBAL_INT:
				{
					debug("MAVLINK_MSG_ID_POSITION_TARGET_GLOBAL_INT\n");
					mavlink_msg_position_target_global_int_decode(&message, &(current_messages.position_target_global_int));
					current_messages.time_stamps.position_target_global_int = get_time_usec();
					this_timestamps.position_target_global_int = current_messages.time_stamps.position_target_global_int;
					printf("time_boot_ms = %u\n"     , current_messages.position_target_global_int.time_boot_ms);
					printf("lat_int = %" PRId32 "\n"     , current_messages.position_target_global_int.lat_int);
					printf("lon_int = %" PRId32 "\n"     , current_messages.position_target_global_int.lon_int);
					printf("vx = %f\n"     , current_messages.position_target_global_int.vx);
					printf("vy = %f\n"     , current_messages.position_target_global_int.vy);
					printf("vz = %f\n"     , current_messages.position_target_global_int.vz);
					printf("afx = %f\n"     , current_messages.position_target_global_int.afx);
					printf("afy = %f\n"     , current_messages.position_target_global_int.afy);
					printf("afz = %f\n"     , current_messages.position_target_global_int.afz);
					printf("yaw = %f\n"     , current_messages.position_target_global_int.yaw);
					printf("yaw_rate = %f\n"     , current_messages.position_target_global_int.yaw_rate);
					printf("type_mask = %u\n"     , current_messages.position_target_global_int.type_mask);
					printf("coordinate_frame = %u\n"     , current_messages.position_target_global_int.coordinate_frame);
					break;
				}

				case MAVLINK_MSG_ID_HIGHRES_IMU:
				{
					debug("MAVLINK_MSG_ID_HIGHRES_IMU\n");
					mavlink_msg_highres_imu_decode(&message, &(current_messages.highres_imu));
					current_messages.time_stamps.highres_imu = get_time_usec();
					this_timestamps.highres_imu = current_messages.time_stamps.highres_imu;
					printf("time_usec = %" PRIu64  "\n"     , current_messages.highres_imu.time_usec);
					printf("xacc = %f\n"     , current_messages.highres_imu.xacc);
					printf("yacc = %f\n"     , current_messages.highres_imu.yacc);
					printf("zacc = %f\n"     , current_messages.highres_imu.zacc);
					printf("xgyro = %f\n"     , current_messages.highres_imu.xgyro);
					printf("ygyro = %f\n"     , current_messages.highres_imu.ygyro);
					printf("zgyro = %f\n"     , current_messages.highres_imu.zgyro);
					printf("xmag = %f\n"     , current_messages.highres_imu.xmag);
					printf("ymag = %f\n"     , current_messages.highres_imu.ymag);
					printf("zmag = %f\n"     , current_messages.highres_imu.zmag);
					printf("abs_pressure = %f\n"     , current_messages.highres_imu.abs_pressure);
					printf("diff_pressure = %f\n"     , current_messages.highres_imu.diff_pressure);
					printf("pressure_alt = %f\n"     , current_messages.highres_imu.pressure_alt);
					printf("temperature = %f\n"     , current_messages.highres_imu.temperature);
					printf("fields_updated = %u\n"     , current_messages.highres_imu.fields_updated);
					printf("id = %u\n"     , current_messages.highres_imu.id);
					break;
				}

				case MAVLINK_MSG_ID_ATTITUDE:
				{
					debug("MAVLINK_MSG_ID_ATTITUDE\n");
					mavlink_msg_attitude_decode(&message, &(current_messages.attitude));
					current_messages.time_stamps.attitude = get_time_usec();
					this_timestamps.attitude = current_messages.time_stamps.attitude;
					printf("time_boot_ms = %u\n"     , current_messages.attitude.time_boot_ms);
					printf("roll = %f\n"     , current_messages.attitude.roll);
					printf("pitch = %f\n"     , current_messages.attitude.pitch);
					printf("yaw = %f\n"     , current_messages.attitude.yaw);
					printf("rollspeed = %f\n"     , current_messages.attitude.rollspeed);
					printf("pitchspeed = %f\n"     , current_messages.attitude.pitchspeed);
					printf("yawspeed = %f\n"     , current_messages.attitude.yawspeed);
					break;
				}

				default:
				{
					// printf("Warning, did not handle message id %i\n",message.msgid);
					break;
				}


			} // end: switch msgid

		} // end: if read message

		// Check for receipt of all items
		received_all =
				this_timestamps.heartbeat                  &&
//				this_timestamps.battery_status             &&
//				this_timestamps.radio_status               &&
//				this_timestamps.local_position_ned         &&
//				this_timestamps.global_position_int        &&
//				this_timestamps.position_target_local_ned  &&
//				this_timestamps.position_target_global_int &&
//				this_timestamps.highres_imu                &&
//				this_timestamps.attitude                   &&
				this_timestamps.sys_status
				;

		// give the write thread time to use the port
		if ( writing_status > false ) {
			usleep(100); // look for components of batches at 10kHz
		}

	} // end: while not received all

	return;
}

// ------------------------------------------------------------------------------
//   Write Message
// ------------------------------------------------------------------------------
int
Autopilot_Interface::
write_message(mavlink_message_t message)
{
	// do the write
	int len = port->write_message(message);

	// book keep
	write_count++;

	// Done!
	return len;
}

// ------------------------------------------------------------------------------
//   Write Setpoint Message
// ------------------------------------------------------------------------------
void
Autopilot_Interface::
write_setpoint()
{
	// --------------------------------------------------------------------------
	//   PACK PAYLOAD
	// --------------------------------------------------------------------------

	// pull from position target
	mavlink_set_position_target_local_ned_t sp;
	{
		std::lock_guard<std::mutex> lock(current_setpoint.mutex);
		sp = current_setpoint.data;
	}

	// double check some system parameters
	if ( not sp.time_boot_ms )
		sp.time_boot_ms = (uint32_t) (get_time_usec()/1000);
	sp.target_system    = system_id;
	sp.target_component = autopilot_id;


	// --------------------------------------------------------------------------
	//   ENCODE
	// --------------------------------------------------------------------------

	mavlink_message_t message;
	mavlink_msg_set_position_target_local_ned_encode(system_id, companion_id, &message, &sp);

	// do the write
	int len = write_message(message);

	// check the write
	if ( len <= 0 )
		fprintf(stderr,"WARNING: could not send POSITION_TARGET_LOCAL_NED \n");
	//	else
	//		printf("%lu POSITION_TARGET  = [ %f , %f , %f ] \n", write_count, position_target.x, position_target.y, position_target.z);

	return;
}


// ------------------------------------------------------------------------------
//   Start Off-Board Mode
// ------------------------------------------------------------------------------
void
Autopilot_Interface::
enable_offboard_control()
{
	// Should only send this command once
	if ( control_status == false )
	{
		printf("ENABLE OFFBOARD MODE\n");

		// ----------------------------------------------------------------------
		//   TOGGLE OFF-BOARD MODE
		// ----------------------------------------------------------------------

		// Sends the command to go off-board
		int success = toggle_offboard_control( true );

		// Check the command was written
		if ( success )
			control_status = true;
		else
		{
			fprintf(stderr,"Error: off-board mode not set, could not write message\n");
			throw EXIT_FAILURE;
		}

		printf("\n");

	} // end: if not offboard_status

}


// ------------------------------------------------------------------------------
//   Stop Off-Board Mode
// ------------------------------------------------------------------------------
void
Autopilot_Interface::
disable_offboard_control()
{

	// Should only send this command once
	if ( control_status == true )
	{
		printf("DISABLE OFFBOARD MODE\n");

		// ----------------------------------------------------------------------
		//   TOGGLE OFF-BOARD MODE
		// ----------------------------------------------------------------------

		// Sends the command to stop off-board
		int success = toggle_offboard_control( false );

		// Check the command was written
		if ( success )
			control_status = false;
		else
		{
			fprintf(stderr,"Error: off-board mode not set, could not write message\n");
			//throw EXIT_FAILURE;
		}

		printf("\n");

	} // end: if offboard_status

}

// ------------------------------------------------------------------------------
//   Arm
// ------------------------------------------------------------------------------
int
Autopilot_Interface::
arm_disarm( bool flag )
{
	if(flag)
	{
		printf("ARM ROTORS\n");
	}
	else
	{
		printf("DISARM ROTORS\n");
	}

	// Prepare command for off-board mode
	mavlink_command_long_t com = { 0 };
	com.target_system    = system_id;
	com.target_component = autopilot_id;
	com.command          = MAV_CMD_COMPONENT_ARM_DISARM;
	com.confirmation     = true;
	com.param1           = (float) flag;
	com.param2           = 21196;

	// Encode
	mavlink_message_t message;
	mavlink_msg_command_long_encode(system_id, companion_id, &message, &com);

	// Send the message
	int len = port->write_message(message);

	// Done!
	return len;
}

// ------------------------------------------------------------------------------
//   Toggle Off-Board Mode
// ------------------------------------------------------------------------------
int
Autopilot_Interface::
toggle_offboard_control( bool flag )
{
	// Prepare command for off-board mode
	mavlink_command_long_t com = { 0 };
	com.target_system    = system_id;
	com.target_component = autopilot_id;
	com.command          = MAV_CMD_NAV_GUIDED_ENABLE;
	com.confirmation     = true;
	com.param1           = (float) flag; // flag >0.5 => start, <0.5 => stop

	// Encode
	mavlink_message_t message;
	mavlink_msg_command_long_encode(system_id, companion_id, &message, &com);

	// Send the message
	int len = port->write_message(message);

	// Done!
	return len;
}


// ------------------------------------------------------------------------------
//   STARTUP
// ------------------------------------------------------------------------------
void Autopilot_Interface::start()
{
	printf("Autopilot_Interface::start()\n");
	int result;

	if ( !port->is_running() )
	{
		fprintf(stderr,"Port Not Open!\n");
		throw 1;
	}

	printf("START READ THREAD \n");

	result = pthread_create( &read_tid, NULL, &start_autopilot_interface_read_thread, this );
	
	printf("Reading Thread Created!\n");

	if ( result ) throw result;
	printf("\n");

	printf("CHECK FOR MESSAGES\n");
	while ( not current_messages.sysid )
	{
		if ( time_to_exit ) return;
		usleep(500000); // check at 2Hz
	}

	printf("System ID Found!\n");

	// now we know autopilot is sending messages
	printf("\n");

	if ( not system_id )
	{
		system_id = current_messages.sysid;
		printf("GOT VEHICLE SYSTEM ID: %i\n", system_id );
	}

	// Component ID
	if ( not autopilot_id )
	{
		autopilot_id = current_messages.compid;
		printf("GOT AUTOPILOT COMPONENT ID: %i\n", autopilot_id);
		printf("\n");
	}

	printf("Querying Position and Attitude!\n");

	while ( not (
		current_messages.time_stamps.local_position_ned &&
		current_messages.time_stamps.attitude
	) ) {
		if ( time_to_exit ) return ;
		usleep(500000);
	}

	printf("INITIAL POSITION XYZ = [ %.4f , %.4f , %.4f ] \n", initial_position.x, initial_position.y, initial_position.z);
	printf("INITIAL POSITION YAW = %.4f \n", initial_position.yaw);
	printf("\n");

	// copy initial position ned
	Mavlink_Messages local_data = current_messages;
	initial_position.x        = local_data.local_position_ned.x;
	initial_position.y        = local_data.local_position_ned.y;
	initial_position.z        = local_data.local_position_ned.z;
	initial_position.vx       = local_data.local_position_ned.vx;
	initial_position.vy       = local_data.local_position_ned.vy;
	initial_position.vz       = local_data.local_position_ned.vz;
	initial_position.yaw      = local_data.attitude.yaw;
	initial_position.yaw_rate = local_data.attitude.yawspeed;

	printf("INITIAL POSITION XYZ = [ %.4f , %.4f , %.4f ] \n", initial_position.x, initial_position.y, initial_position.z);
	printf("INITIAL POSITION YAW = %.4f \n", initial_position.yaw);
	printf("\n");

	printf("START WRITE THREAD \n");

	result = pthread_create( &write_tid, NULL, &start_autopilot_interface_write_thread, this );
	if ( result ) throw result;

	while ( not writing_status )
		usleep(100000); // 10Hz

	printf("\n");

	printf("Completed autopilot start!\n");

	// Done!
	return;

}


// ------------------------------------------------------------------------------
//   SHUTDOWN
// ------------------------------------------------------------------------------
void
Autopilot_Interface::
stop()
{
	printf("CLOSE THREADS\n");

	// signal exit
	time_to_exit = true;

	// wait for exit
	pthread_join(read_tid ,NULL);
	pthread_join(write_tid,NULL);

	// now the read and write threads are closed
	printf("\n");

	// still need to close the port separately
}
void
Autopilot_Interface::
start_read_thread()
{

	if ( reading_status != 0 )
	{
		fprintf(stderr,"read thread already running\n");
		return;
	}
	else
	{
		read_thread();
		return;
	}

}

void
Autopilot_Interface::
start_write_thread(void)
{
	if ( not writing_status == false )
	{
		fprintf(stderr,"write thread already running\n");
		return;
	}

	else
	{
		write_thread();
		return;
	}

}

void
Autopilot_Interface::
handle_quit( int sig )
{

	disable_offboard_control();

	try {
		stop();

	}
	catch (int error) {
		fprintf(stderr,"Warning, could not stop autopilot interface\n");
	}

}

void
Autopilot_Interface::
read_thread()
{
	reading_status = true;

	while ( ! time_to_exit )
	{
		read_messages();
		usleep(100000); // Read batches at 10Hz
	}

	reading_status = false;

	return;
}

void
Autopilot_Interface::
write_thread(void)
{
	// signal startup
	writing_status = 2;

	// prepare an initial setpoint, just stay put
	mavlink_set_position_target_local_ned_t sp;
	sp.type_mask = MAVLINK_MSG_SET_POSITION_TARGET_LOCAL_NED_VELOCITY &
				   MAVLINK_MSG_SET_POSITION_TARGET_LOCAL_NED_YAW_RATE;
	sp.coordinate_frame = MAV_FRAME_LOCAL_NED;
	sp.vx       = 0.0;
	sp.vy       = 0.0;
	sp.vz       = 0.0;
	sp.yaw_rate = 0.0;

	// set position target
	{
		std::lock_guard<std::mutex> lock(current_setpoint.mutex);
		current_setpoint.data = sp;
	}

	// write a message and signal writing
	write_setpoint();
	writing_status = true;

	// Pixhawk needs to see off-board commands at minimum 2Hz,
	// otherwise it will go into fail safe
	while ( !time_to_exit )
	{
		usleep(250000);   // Stream at 4Hz
		write_setpoint();
	}

	// signal end
	writing_status = false;

	return;

}

void*
start_autopilot_interface_read_thread(void *args)
{
	// takes an autopilot object argument
	Autopilot_Interface *autopilot_interface = (Autopilot_Interface *)args;

	// run the object's read thread
	autopilot_interface->start_read_thread();

	// done!
	return NULL;
}

void*
start_autopilot_interface_write_thread(void *args)
{
	// takes an autopilot object argument
	Autopilot_Interface *autopilot_interface = (Autopilot_Interface *)args;

	// run the object's read thread
	autopilot_interface->start_write_thread();

	// done!
	return NULL;
}
