# Drone Swarm

My work on the drone swarm.

## Plan

1. Make mavlink reader thread handling.
(Writer will be able to query reader's status through messages structure!)
2. Create mavlink mavlink property reading functions.
3. Create mavlink writer functions.
4. Create test application which calls mavlink reader
then performs some mavlink status checks then performs
mavlink write.

# Notes:

* Pixhawk should be periodically sending information out.
* Offboard control must be enabled before issuing commands to the pixhawk over mavlinkoffboard control must be enabled before issuing commands to the pixhawk over mavlink.
